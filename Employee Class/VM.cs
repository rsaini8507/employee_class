﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Employee_Class
{
    public class VM : INotifyPropertyChanged
    {
        public BindingList<Employee> Employees { get; set; } = new BindingList<Employee>() {
            new Employee("John Smith", 101, "Accounting", "Manager", "John Smith.png"),
            new Employee("Mike Johnson", 102, "Mike Johnson.png"),
            new Employee("Bella Miller", "Marketing", "Sales Representative","Bella Miller.ico"),
        };

        public Employee selectedEmployee = null;
        public Employee SelectedEmployee
        {
            get { return selectedEmployee; }
            set { selectedEmployee = value; NotifyChanged(); }
        }

        public string lblCount = "";
        public string LblCount
        {
            get { return lblCount; }
            set { lblCount = value; NotifyChanged(); }
        }

        public int txtId = 0;
        public int TxtId
        {
            get { return txtId; }
            set { txtId = value; NotifyChanged(); }
        }

        public string txtName = "";
        public string TxtName
        {
            get { return txtName; }
            set { txtName = value; NotifyChanged(); }
        }

        public string txtDepartment = "";
        public string TxtDepartment
        {
            get { return txtDepartment; }
            set { txtDepartment = value; NotifyChanged(); }
        }

        public string txtPosition = "";
        public string TxtPosition
        {
            get { return txtPosition; }
            set { txtPosition = value; NotifyChanged(); }
        }

        public string imgUpload = "";
        public string ImgUpload
        {
            get { return imgUpload; }
            set { imgUpload = value; NotifyChanged(); } 
        }

        public string inputKeywords = "";
        public string InputKeywords
        {
            get { return inputKeywords; }
            set { inputKeywords = value; NotifyChanged(); }
        }

        public void CountEmployee()
        {
            LblCount = "Total Employee Count: " + Employees.Count();
        }

        public void CreateEmployee()
        {
            Employees.Add(new Employee
            {
                Name = TxtName,
                IdNumber = TxtId,
                Department = TxtDepartment,
                Position = TxtPosition,
                ImageSource = ImgUpload
            });
            Employees.
        }

        public void ClearInput()
        {
            TxtName = "";
            TxtId = 0;
            TxtDepartment = "";
            TxtPosition = "";
            ImgUpload = "";
        }

        public void UploadPicture()
        {
            try{
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "jpg files(*.jpg;*jpeg)|*.jpg;*.jpeg| PNG files(*.png)|*.png";
                if(dialog.ShowDialog() == true)
                {
                    ImgUpload = dialog.FileName;
                }
            }
            catch
            {
                MessageBox.Show("An error occured", "Error", MessageBoxButton.OK,MessageBoxImage.Error);
            }            
        }

        public void DeleteEmployee()
        {
            if(SelectedEmployee == null)
            {
                MessageBox.Show("Please select one item.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (MessageBox.Show("Are you Sure to delete the selected item?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    Employees.RemoveAt(Employees.IndexOf(SelectedEmployee));
                }               
            }            
        }

        public bool Decide()
        {
            if(selectedEmployee == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion

    }
}
