﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee_Class
{
    public class Employee
    {
        public string Name { get; set; }
        public int IdNumber { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public string ImageSource { get; set; }
        public string Color { get; set; } = "lightblue";


        public Employee()
        {
            Name = "Admin";
            IdNumber = 1;
            Department = "";
            Position = "";
            ImageSource = "";
        }

        public Employee(string name, int id, string department, string position, string imagesource)
        {
            Name = name;
            IdNumber = id;
            Department = department;
            Position = position;
            ImageSource = imagesource;
        }
        public Employee(string name, int id, string imagesource)
        {
            Name = name;
            IdNumber = id;
            Department = "";
            Position = "";
            ImageSource = imagesource;
        }
        public Employee(string name, string department, string position, string imagesource)
        {
            Name = name;
            IdNumber = 0;
            Department = department;
            Position = position;
            ImageSource = imagesource;
        }

        public override string ToString() => $"{Name} | {IdNumber}";
    }
}
