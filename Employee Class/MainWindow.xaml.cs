﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Employee_Class
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        CreateEmployee ce = null;
        EmployeeDetails ew = null;
        VM vm = new VM();
        public MainWindow()
        {
            InitializeComponent();
            DataContext = vm;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            vm.CountEmployee();
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(ew == null && vm.Decide())
            {
                ew = new EmployeeDetails(vm)
                {
                    Owner = this,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner
                };
                ew.Closed += Ew_Closed;
                ew.Show();              
            }
        }

        private void Ew_Closed(object sender, EventArgs e)
        {
            ew = null;
        }

        private void BtnCreate_Click(object sender, RoutedEventArgs e)
        {
            if (ce == null)
            {
                ce = new CreateEmployee(vm)
                {
                    Owner = this,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner
                };
                ce.Closed += Ce_Closed;
                ce.ShowDialog();
                vm.CountEmployee();
            }
        }

        private void Ce_Closed(object sender, EventArgs e)
        {
            ce = null;
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            vm.DeleteEmployee();
            vm.CountEmployee();
        }
    }
}
