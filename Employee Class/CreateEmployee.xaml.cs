﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Employee_Class
{
    /// <summary>
    /// Interaction logic for CreateEmployee.xaml
    /// </summary>
    public partial class CreateEmployee : Window
    {
        VM vm = null;
        public CreateEmployee(VM vm)
        {
            this.vm = vm;
            InitializeComponent();
            DataContext = vm;
        }

        private void BtnCreate_Click(object sender, RoutedEventArgs e)
        {
            vm.CreateEmployee();
            MessageBox.Show("Create employee Successfully.","Information");
            vm.ClearInput();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            vm.ClearInput();
        }

        private void BtnUpload_Click(object sender, RoutedEventArgs e)
        {
            vm.UploadPicture();
        }
    }
}
